package com.example.biblan.Controller;

import com.example.biblan.Entity.Book;
import com.example.biblan.Repository.BookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "/api/v1/library/book")
@CrossOrigin
public class LibrarianController {

    @Autowired
    BookRepo bookRepo;

    @GetMapping()
    public Iterable<Book> getBook() {
        return bookRepo.findAll();
    }


    @PostMapping
    public Book save(@RequestBody Book book){
        return bookRepo.save(book);
    }

    @DeleteMapping("{id}")
    public void deleteBook(@PathVariable Long id) {
        try{
            bookRepo.deleteById(id);
        } catch(NumberFormatException ex){
        }
    }

    @PutMapping("/borrow/{id}")
    public Optional<Book> borrow(@PathVariable Long id){
        return bookRepo.findById(id).map(book -> {
            book.setAvailable(false);
            return bookRepo.save(book);
        });
    }

    @PutMapping("/return/{id}")
    public Optional<Book> returnBook(@PathVariable Long id){
        return bookRepo.findById(id).map(book -> {
            book.setAvailable(true);
            return bookRepo.save(book);
        });
    }

    /**
     * Låna bok
     * - LÅNA BOK: När en låntagare ska låna en specifik bok baserat på id så vill vi först kolla is_available i databasen
     * Om boken är available, dvs satt is_available är TRUE > då ska man kunna hämta den boken, och is_avaialable ska sättas till false;
     *
     * ÅTERLÄMNA BOK: När en låntagare ska lämna bok så sätter vi endast is_available till true igen.
     * METOD: POST
     *
     *  Sta fali --->
     *
     *  Alla REST-metoder skall returnera korrekt HTTP Statuskod
     *  Det ska finnas säkerhet i form av autentisering/inloggning och ACL
     *  -Det ska gå att filtrera och sortera sökresultatet
     *
     * GET requests för sökning av böcker skall vara tillåtet som anonym
     * Alla andra requests skall kräva autentisering
     * Alla lösenord skall krypteras med BCrypt
     * Lösenord skall aldrig skickas från er backend oavsett API-anrop
     * Som inloggad admin skall man kunna köra alla REST-metoder
     *
     * inspiracija https://github.com/chromohh
     *
     *  nazovi me kad budes kuci
     */
}
