package com.example.biblan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class BiblanApplication {

	public static void main(String[] args) {
		SpringApplication.run(BiblanApplication.class, args);
	}

}
